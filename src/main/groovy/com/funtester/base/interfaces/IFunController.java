package com.funtester.base.interfaces;

public interface IFunController extends Runnable {

    public void add();

    public void reduce();

    public void over();

}
