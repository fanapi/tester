package com.funtester.config

class PoolConstant {

    public static int MAX = 200;

    public static int MAX_IDLE = 88;

    public static int MIN_IDLE = 8;

    public static int MAX_WAIT_TIME = 1024;

    public static int MAX_IDLE_TIME = 8 * 60 * 1000;


}
