package com.funtester.frame.thread;

import com.funtester.base.constaint.FixedQps;
import com.funtester.base.interfaces.MarkRequest;
import com.funtester.httpclient.FunHttp;
import com.funtester.httpclient.GCThread;
import org.apache.hc.client5.http.classic.methods.HttpUriRequestBase;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class RequestTimeFixedQps extends FixedQps<HttpUriRequestBase> {

    private static final long serialVersionUID = -64206522585960792L;

    private static Logger logger = LogManager.getLogger(RequestTimeFixedQps.class);

    private RequestTimeFixedQps() {

    }

    public RequestTimeFixedQps(int qps, int time, MarkRequest markRequest, HttpUriRequestBase request) {
        super(request, time, qps, markRequest, false);
    }

    @Override
    public void before() {
        super.before();
        GCThread.starts();
    }

    @Override
    protected void doing() throws Exception {
        FunHttp.executeSimlple(f);
    }

    @Override
    public RequestTimeFixedQps clone() {
        RequestTimeFixedQps newone = new RequestTimeFixedQps();
        newone.f = f;
        newone.mark = this.mark == null ? null : this.mark.clone();
        newone.qps = this.qps;
        newone.isTimesMode = this.isTimesMode;
        newone.limit = this.limit;
        return newone;
    }


}
