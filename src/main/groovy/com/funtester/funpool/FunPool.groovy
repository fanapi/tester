package com.funtester.funpool

import java.util.concurrent.LinkedBlockingQueue
import java.util.function.Supplier

/**
 * Object pool, using LinkedBlockingQueue to store objects, and using factory to create new objects, and using offer to return objects, and using poll to borrow objects, and using trimQueue to trim queue size
 * @param <F>
 */
class FunPool<F> {

    Supplier<F> supplier

    /**
     * Object pool, using LinkedBlockingQueue to store objects
     */
    LinkedBlockingQueue<F> pool = new LinkedBlockingQueue<F>()

    FunPool(Supplier<F> supplier) {
        this.supplier = supplier
    }

    /**
     * Borrow an object from the pool
     *
     * @param o the object to be borrowed
     * @return
     */
    F borrow() {
        F f = pool.poll()
        if (f == null) {
            f = supplier.get()
        }
        return f
    }

    /**
     * Return the object to the pool
     *
     * @param f the object to be returned
     */
    def back(F f) {
        boolean offer = pool.offer(f)
        if (!offer) f = null
    }

    /**
     * return size of object pool
     *
     * @return
     */
    int size() {
        return pool.size()
    }


    /**
     * Trim the queue size
     * @param size the size to be trimmed
     */
    def trimQueue(int size) {
        while (true) {
            if (size() <= size) break
            pool.poll()
        }
    }


}
