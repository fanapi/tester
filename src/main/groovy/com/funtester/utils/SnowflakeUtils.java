package com.funtester.utils;

public class SnowflakeUtils {

    private static final long START_TIMESTAMP = 1616489534000L; // 起始时间戳，2021-03-23 00:00:00

    private long datacenterId; // 数据中心ID

    private long workerId;     // 机器ID

    private long sequence = 0L; // 序列号

    private static final long MAX_WORKER_ID = 31L;// 机器ID最大值

    private static final long MAX_DATA_CENTER_ID = 31L;// 数据中心ID最大值

    private static final long SEQUENCE_BITS = 12L;// 序列号位数

    private static final long WORKER_ID_SHIFT = SEQUENCE_BITS;// 机器ID左移位数

    private static final long DATA_CENTER_ID_SHIFT = SEQUENCE_BITS + WORKER_ID_SHIFT;// 数据中心ID左移位数

    private static final long TIMESTAMP_LEFT_SHIFT = DATA_CENTER_ID_SHIFT + DATA_CENTER_ID_SHIFT;// 时间戳左移位数

    private static final long SEQUENCE_MASK = ~(-1L << SEQUENCE_BITS);// 序列号掩码

    private long lastTimestamp = -1L;

    public SnowflakeUtils(long datacenterId, long workerId) {
        if (datacenterId > MAX_DATA_CENTER_ID || datacenterId < 0) {
            throw new IllegalArgumentException("Datacenter ID can't be greater than " + MAX_DATA_CENTER_ID + " or less than 0");
        }
        if (workerId > MAX_WORKER_ID || workerId < 0) {
            throw new IllegalArgumentException("Worker ID can't be greater than " + MAX_WORKER_ID + " or less than 0");
        }
        this.datacenterId = datacenterId;
        this.workerId = workerId;
    }

    /**
     * 获取下一个ID
     *
     * @return
     */
    public synchronized long nextId() {
        long timestamp = System.currentTimeMillis();
        if (timestamp < lastTimestamp) {
            if (this.lastTimestamp - timestamp >= 2000L)
                throw new RuntimeException("时钟向后移动，拒绝生成ID，时间差" + (lastTimestamp - timestamp) + "毫秒");
            else timestamp = this.lastTimestamp;
        }
        // 将上次生成ID的时间戳 赋值给 当前时间戳
        timestamp = this.lastTimestamp;
        if (lastTimestamp == timestamp) {
            sequence = (sequence + 1) & SEQUENCE_MASK;
            if (sequence == 0) {
                timestamp = nextMillis(lastTimestamp);
            }
        } else {
            sequence = 0L;
        }
        lastTimestamp = timestamp;
        long l = ((timestamp - START_TIMESTAMP) << TIMESTAMP_LEFT_SHIFT) | (datacenterId << DATA_CENTER_ID_SHIFT) | (workerId << WORKER_ID_SHIFT) | sequence;
        return l & Long.MAX_VALUE;
    }

    /**
     * 获取下一个时间戳
     *
     * @param lastTimestamp
     * @return
     */
    private long nextMillis(long lastTimestamp) {
        long timestamp = System.currentTimeMillis();
        while (timestamp <= lastTimestamp) {
            timestamp = System.currentTimeMillis();
        }
        return timestamp;
    }

}
